-- Drop database
DROP DATABASE music_db;

-- create database
CREATE DATABASE music_db;

-- select a database
USE music_db;


INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("Lady Gaga");
INSERT INTO artists(name) VALUES ("Justin Bieber");
INSERT INTO artists(name) VALUES ("Ariana Grande");
INSERT INTO artists(name) VALUES ("Bruno Mars");


INSERT INTO artists(name) VALUES ("Willie Revillame");
INSERT INTO artists(name) VALUES ("G***** Rapper");
INSERT INTO artists(name) VALUES ("The Last Shadow Puppets");
INSERT INTO artists(name) VALUES ("Tupac Amaru Shakur");
INSERT INTO artists(name) VALUES ("Eminem");
INSERT INTO artists(name) VALUES ("Blink");

-- Taylor Swift
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless", "2008-01-01", 1);


INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Fearless", 256, "Pop rock", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Lover", 246, "Pop rock", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Red", "2012-01-01", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("White Horse", 250, "Country Pop", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("State of Grace", 250, "Rock, alternative rock, arena rock", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 2);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("A Star is Born", "2018-01-01", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Halik cover", 200, "Rock", 3);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Black Eyes", 202, "Rock", 3);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Shallow", 202, "Country Rock", 3);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Purpose", "2015-01-01", 3);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Sorry", 212, "Dancehall-poptropical housemoombathon", 4);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Boom tarat tarat - cover", 500, "Pop", 4);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Believe", "2012-01-01", 4);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 5);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-01-01", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 6);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-01-01", 4); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 216, "Pop, R&B", 7);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-01-01", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Magbalik - Cover", 207, "Rock", 8);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-01-01", 5);


-- Exclude records
SELECT = FROM songs WHERE id != 5
SELECT = FROM albums WHERE id !=6

-- Greater than / less than
SELECT = FROM songs WHERE album_id > 1
SELECT = FROM songs WHERE album_id < 8

-- Between
SELECT = FROM songs WHERE album_id BETWEEN 2 AND 7

-- GET SPECIFIC IDs (OR)
SELECT = FROM songs WHERE id = 1 OR id = 2 OR id = 5;


-- GET SPECIFIC IDs (IN)
SELECT = FROM songs WHERE id IN (1, 2, 5);
SELECT = FROM songs WHERE genre IN ("Pop", "Dancehall-poptropical housemoombathon", "Rock");

-- combining conditions
SELECT * FROM WHERE genre = "Rock" AND album_id = 3;

-- finding partial matches
SELECT * FROM songs WHERE song_name LIKE "%d"; -- all songs that end in d


-- ends with
SELECT * FROM songs WHERE song_name LIKE "%d";
-- starts with
SELECT * FROM songs WHERE song_name LIKE "Lo%";
SELECT * FROM songs WHERE song_name LIKE "%a%";
SELECT * FROM songs WHERE song_name LIKE "l%v%y"; -- starts with letter l, have v in between, and ends with y

SELECT * FROM albums WHERE date_released LIKE "201_"

SELECT * FROM songs WHERE song_name LIKE "Lov__" -- know some keywords
SELECT * FROM songs WHERE song_name LIKE "__er"
SELECT * FROM songs WHERE length "__:_2:__";


-- sort records
-- numbers (0 - infinity)
SELECT * FROM songs ORDER BY song_name -- default is ascending
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;


-- getting distincting records
SELECT DISTINCT genre FROM songs

-- additional example
SELECT * FROM songs WHERE genre LIKe "%Pop%"

-- [SECTION] Table Joins

-- INNER JOIN
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;

-- LEFT JOIN
SELECT * FROM artists LEFT JOIN albums ON artists.id = albums.artist_id;

-- RIGHT JOIN
SELECT * FROM artists RIGHT JOIN albums ON artists.id = albums.artist_id;


-- combine more than two tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id